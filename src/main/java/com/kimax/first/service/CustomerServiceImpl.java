package com.kimax.first.service;

import java.util.List;

import com.kimax.first.model.Customer;
import com.kimax.first.repository.CustomerRepository;
import com.kimax.first.repository.HibernateCustomerRepositoryImpl;

public class CustomerServiceImpl implements CustomerService {

	private CustomerRepository customerRepository = new HibernateCustomerRepositoryImpl();
	
	/* (non-Javadoc)
	 * @see com.kimax.first.service.CustomerService#findAll()
	 */
	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
