package com.kimax.first.service;

import java.util.List;

import com.kimax.first.model.Customer;

public interface CustomerService {

	List<Customer> findAll();

}