package com.kimax.first.repository;

import java.util.ArrayList;
import java.util.List;

import com.kimax.first.model.Customer;

public class HibernateCustomerRepositoryImpl implements CustomerRepository {

	/* (non-Javadoc)
	 * @see com.kimax.first.repository.CustomerRepository#findAll()
	 */
	@Override
	public List<Customer> findAll() {
		List<Customer> customers = new ArrayList<>();
		
		Customer  customer = new Customer();		
		customer.setFirstName("Dereck");
		customer.setLastName("Abouem");
		
		customers.add(customer);
		
		return customers;
	}
}
