package com.kimax.first.repository;

import java.util.List;

import com.kimax.first.model.Customer;

public interface CustomerRepository {

	List<Customer> findAll();

}